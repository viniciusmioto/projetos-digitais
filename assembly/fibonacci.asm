.data
msg1:	.asciiz "Entre com um termo para calcular: "
msg2: 	.asciiz "O resultado é: "

.text
	# imprime a mensagem 1
	li $v0, 4
	la $a0, msg1
	syscall
	
	# recebe um número do teclado
	li $v0, 5
	syscall
	
	# chama a função fib
	move $a0, $v0
	jal fibRec
	move $s0, $v0
	
	# imprime o resultado na tela
	li $v0, 4
	la $a0, msg2
	syscall
	move $a0, $s0
	li $v0, 1
	syscall

	# termina a execução
	li $v0, 10
	syscall


# i -> t0; n1 -> t1; n2 -> t2; next -> t3, n -> a0
fib:
	addi $t1, $zero, 1		# n1 = 1			
	addi $t2, $zero, 1		# n2 = 1
	addi $t0, $zero, 2		# i = 2
for:
	bge $t0, $a0, fimFor 		# condição de parada
	add $t3, $t1, $t2		# next = n1 + n2;
	move $t1, $t2			# n1 = n2;
	move $t2, $t3			# n2 = next;
	addi $t0, $t0, 1		# i++
	j for
	
fimFor:
	move $v0, $t2			# retorno v0 = t1
	jr $ra
	
fibRec:
	addi $t0, $zero, 1
	bne $a0, $t0, depoisIF
	addi $v0, $zero, 1
	jr $ra

depoisIF:
	addi $t0, $zero, 2
	bne $a0, $t0, depoisIF2
	addi $v0, $zero, 1
	jr $ra
depoisIF2:
	addi $sp, $sp, -12
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	
	addi $a0, $a0, -1
	jal fibRec
	add $t0, $zero, $v0	
	sw $t0, 8($sp)
	
	lw $a0, 4($sp)
	addi $a0, $a0, -2
	jal fib
	add $t1, $zero, $v0
	
	lw $t0, 8($sp)
	add $v0, $t0, $t1
	
	lw $ra, 0($sp)
	addi $sp, $sp, 12
	jr $ra
	
	
	
	
	
	