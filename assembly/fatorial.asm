# $a0 recebe o argumento N
# $t0 -> variavel f, que acumula o produto
# $t1 -> variavel i, vai de N ate 1

.data
msg1:	.asciiz "Entre com um numero: "
msg2: 	.asciiz "Fatorial do numero: "

.text
	li $v0, 4			# imprime mensagem
	la $a0, msg1
	syscall
	
	li $v0, 5			# ler numero do teclado
	syscall
	
	addi $a0, $v0, 0		# valor do argumento da funcao
	jal fatRec			# chama a funcao fat
	addi $s0, $v0, 0		# salva o resultado
	
	li $v0, 4			# imprime mensagem
	la $a0, msg2
	syscall
	
	addi $a0, $s0, 0		# coloca o resultado de fat em a0
	li $v0, 1			# imprime resultado na tela
	syscall

	li $v0, 10
	syscall


# int fat (int n) {
#	int i, f=1;
#	for (i=n; i>1; i--)
#		f = f*i;
#	return f;		
#}
.text
fat: 
	addi $t0, $zero, 1		# f = 1
	addi $t2, $a0, 0		# t2 = n
	addi $t1, $zero, 1		# t1 = 1, para o beq
	
for:
	bgt $t1, $t2, fimFor		# verifica se i < 15
	mul $t0, $t0, $t1		# f = f * i
	addi $t1, $t1, 1		# i++
	j for				# volta para o for
	
fimFor:
	addi $v0, $t0, 0		# terminou for
	jr $ra

#int fact_rec(int n)
#{
#    if (n == 0)
#        return 1;
#    return n * fact_rec(n - 1);
#}	
				
fatRec:
	ble $a0, 1, retorno			# if (n < 1)
	addi $sp, $sp, -8			# reserva espaço na pilha
	sw $ra, 0($sp)				# aloca o ende de retorno $ra
	sw $a0, 4($sp)				# aloca o argumento $a0
	addi $a0, $a0, -1			# n = n - 1
	jal fatRec
	
	lw $ra, 0($sp)				# carrega end de ret da pilha
	lw $a0, 4($sp)				# carrega o argumento da pilha
	addi $sp, $sp, 8			# devolve espaço para a pilha
	
	mul $t0, $a0, $v0			# n * f(n-1)
	move $v0, $t0				# define valor do retorno
	jr $ra
retorno:
	move $v0, $a0
	jr $ra
