.data
P:	.word	5 12 0 7 3 4 9 31 19 1 17 2 8 40 13
tamP:	.word	15
Q:	.word	7 63 20 5 3 8 1 14 28 33 7 75 12 0 65
tamQ:	.word	15

m1:	.asciiz  "O vetor P:\n"
m2:	.asciiz  "O vetor Q:\n"
.text

	la $t0, P		# endereço base de P[0] para o reg t0
	la $t1, Q		# endereço base de Q[0] para o reg tq
	
	lw $t2, 16($t0)		# t2 <- P[4] | i = P[4]
	lw $t3, 36($t0)		# t3 <- P[9] | j = P[9]
	
	sub $t4, $t2, $t3	# t4 <- t2 - t3 | k = i - j
	
	sll $t9, $t2, 2		# t9 <- i * 4 | endereço
	add $t9, $t9, $t1	# t9 <- endereço base + deslocamento | t9 = &Q[P[4]]
	lw $t5, 0($t9)		# t5 <- P[4] | y = Q[i]

	sll $t9, $t2, 4		# t9 <- [i * 4] * 4 | endereço
	add $t9, $t9, $t1	# t9 <- endereço base + deslocamento | t9 = &Q[P[4] * 4]
	lw $t6, 0($t9)		# t5 <- P[4] | z = Q[i * 4]
	
	add $t7, $t2, $t3	# t7 <- t2 + t3 | t7 = i + j
	sll $t7, $t7, 2		# t7 <- (i + j) * 4 bytes
	add, $t7, $t7, $t1	# t7 <- base de Q + (i + j) * 4 | t7 = &Q[i + j]
	lw $t7, 0($t7)		# t7 = Q[i + j]
	
	# y + z + Q[i + j]
	add $t7, $t7, $t6	# t7 += z
	add $t7, $t7, $t5	# t7 += y
	
	sll $t9, $t4, 2		# t9 = k * 4 bytes
	add $t9, $t9, $t0	# t9 = &P[k]
	lw $t9, 0($t9)		# t9 = P[k]
	sll $t9, $t9, 2		# t9 = P[k] * 4 bytes
	add $t9, $t9, $t0	# t9 = &P[P[k]]
	sw $t7, 0($t9)		# t9 <- t7 | P[P[k]] = y + z + Q[i + j]


	# imprime o vetor P na tela
	la   $a0, m1       
	li   $v0, 4        
	syscall     
	la   $a0, P        
	la   $a1, tamP
	lw   $a1, 0($a1)
	jal  print      
	
	# imprime o vetor Q na tela
	la   $a0, m2       
	li   $v0, 4        
	syscall
	la   $a0, Q        
	la   $a1, tamQ
	lw   $a1, 0($a1)
	jal  print         
	
	# Encerrar o programa.
	li   $v0, 10       
	syscall   
	
###############################################################
# Funcao para imprimir numeros (vetor) em uma linha
# Nao se preocupem em entender isso aqui
.data
espaco:	.asciiz  " "          # espaco entre numeros
nl:	.asciiz  "\n"         # quebra de linha (nova linha)

.text
print:	add  $t0, $zero, $a0  # endereco inicial do vetor, deve estar em $a0
	add  $t1, $zero, $a1  # inicializa um contador com o tamanho do vetor

      
out:  	lw   $a0, 0($t0)      # carrega o numero atual do vetor para ser impresso
	li   $v0, 1           # define funcao para imprimir inteiro
	syscall               # chama funcao para imprimir inteiro
      
	# imprime um espaco
	la   $a0, espaco      
	li   $v0, 4           
	syscall               
      
	addi $t0, $t0, 4      # incrementa o endereco base para o proximo numero no vetor
	addi $t1, $t1, -1     # decrementa o contador
	bgtz $t1, out         # se o contador nao eh zero, repete tudo para cima

	# imprime uma nova linha, quebra a linha
	la   $a0, nl          
	li   $v0, 4           
	syscall               
            
	jr   $ra              # retorna da funcao
###############################################################
