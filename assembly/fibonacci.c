#include <stdio.h>

/* Receives a number and returns the nth fibonacci number. Calculates it iteratively */
int fibonacci(int n) 
{
    int i, n1, n2, next;
    n1 = 0;
    n2 = 1;

    if (n == 0)
        return n2;
    if (n == 1)
        return n2;

    for (i = 0; i < n; i++)
    {
        next = n1 + n2;
        n1 = n2;
        n2 = next;
    }

    return n2;
}

/* Receives a number and returns the nth fibonacci number. Calculates it recursively */ 
int fibonacci_recursive(int n) 
{
    if (n == 0)
        return 0;
    if (n == 1)
        return 1;

    return fibonacci_recursive(n - 1) + fibonacci_recursive(n - 2);
}

int main()
{
    int i;
    /* print first 10 fibonacci numbers */
    for (int i = 0; i < 10; i++)
        printf("%d, ", fibonacci(i));

    printf("\n");

    /* print first 10 fibonacci numbers */
    for (int i = 0; i < 10; i++)
        printf("%d, ", fibonacci_recursive(i));

    printf("\n");
    return 0;
}