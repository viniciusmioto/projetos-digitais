#include <stdio.h>

/* Receives a number (n) and returns the factorial of it, n! */
int factorial(int n)
{
    int i, fact = 1;

    for (i = 1; i <= n; i++)
        fact *= i;

    return fact;
}

/* Receives a number (n) and returns the factorial of it, n! Recursively */
int factorial_recursive(int n)
{
    if (n == 0)
        return 1;

    return n * factorial_recursive(n - 1);
}

int main()
{
    printf("%d\n", factorial(5));
    printf("%d\n", factorial_recursive(5));
    return 0;
}
