def f2(a):
    return 2 + a * 3

def f1(x, y):
    x = x * y + f2(x)
    return x

print(f1(3, 5))
