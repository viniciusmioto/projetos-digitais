.text
# a0 <- A
# a1 <- B
# a2 <- C
# a3 <- D


addi $t4, $zero, 2		# X = 2
addi $t5, $zero, 3		# Y = 3
addi $t6, $zero, 4		# Z = 4
addi $t7, $zero, 38		# W = 38

move $a0, $t4			# parametro A = X

move $a1, $t5			# parametro B = Y

addi $a2, $t5, -2		# parametro C = Y - 2
add $a2, $a2, $t4 		# parametro C = C + x

add $a3, $t6, $t7 		# parametro D = Z + W
sub $a3, $a3, $t5 		# parametro D = D - Y

jal fun				# salta para fun

move $s0, $v0			# salva o resultado
move $a0, $s0			# coloca o resultado de fat em a0
li $v0, 1			# imprime resultado na tela
syscall
	
li $v0, 10			# encerra a execução
syscall
	
fun:
	ble $a0, $a1, else
	sll $t1, $a3, 1		# T1 = D * 2
	add $t1, $t1, $a0	# T1 = T1 + A
	move $a2, $t1		# C = T1
	j valor
	
else:
	sll $t2, $a3, 2		# T2 = D * 4
	add $t2, $t2, $a1	# T2 = T2 + B
	move $a2, $t2		# C = T2
	j valor
	
	
valor:
	mul $t3, $a2, $a3	# T3 = C * D
	addi $t3, $t3, 8	# T3 = T3 + 8
	move $a2, $t3		# C = T3
	move $v0, $a2		# define o valor de retorno
	jr $ra
