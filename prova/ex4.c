#include <stdio.h>

int fun(int a, int b, int c, int d){
    if (a>b){
        printf("IF\n");
        c = 2*d + a;
    }else{
        printf("ELSE\n");
        c = 4*d + b;
    }
    c = c*d + 8;

    return c;
}

int main(){
    int x, y, z, w;
    x = 10;
    y = 3;
    z = 4;
    w = 38;
    printf("Values: %d %d %d %d\n", x, y, x + y - 2, z + w - y);
    printf("%d\n", fun(x, y, x + y - 2, z + w - y));

    return 0;
}