# Exercício 3

# int i, j = 5, s = 1;
# for (i = 0; i < j; i++)
#	s += i

# t0 -> variavel i
# t1 -> variavel j
# t2 -> variavel s

.text
	
	addi $t0, $zero, 0		# i = 0
	addi $t1, $zero, 5		# j = 5
	addi $t2, $zero, 1		# s = 1

for:	bge $t0, $t1, fim		# verifica se i >= j
	add $t2, $t2, $t0		# s += i
	addi $t0, $t0, 1		# i++
	j for

fim:	
	addi $a0, $t2, 0		# coloca o valor de s em a0
	li $v0, 1			# imprime s na tela
	syscall
