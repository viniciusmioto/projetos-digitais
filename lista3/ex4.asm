# Exercício 4

# int i = -5, s = 1;
# while (s < 120){
# 	i = i + 1;
# 	s = s + i;
# }

# t0 -> variavel i
# t1 -> variavel j
# t2 -> 120 (while)

.data
nl:	.asciiz "\n"

.text
	
	addi $t0, $zero, -5		# i = -5
	addi $t1, $zero, 1		# s = 1
	addi $t2, $zero, 120

for:	bge $t1, $t2, fim		# verifica se s >= 120
	add $t0, $t0, 1			# i += 1
	add $t1, $t1, $t0		# s += i
	j for

fim:	
	addi $a0, $t0, 0		# coloca o valor de i em a0
	li $v0, 1			# imprime i na tela
	syscall
	
	li $v0, 4			# imprime mensagem
	la $a0, nl			# pula linha
	syscall
	
	addi $a0, $t1, 0		# coloca o valor de s em a0
	li $v0, 1			# imprime s na tela
	syscall
