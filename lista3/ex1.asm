# Exercício 1. Implemente em assembly do MIPS um código que soma todos os números entre 5 e 15

# soma = 0;
# for (i=5, i<=15, i++)
# 	soma += soma + i;

# t0 -> variavel que armazena a soma
# t1 -> variavel i, inicializada em 5
# t2 -> contém o valor 15 para interromper o loop

.data
nl:	.asciiz "\n"
pipe:	.asciiz " | "

.text
	
	addi $t1, $zero, 5		# i = 5
	addi $t2, $zero, 15		# t2 = 15

for:
	bgt $t1, $t2, fimFor		# verifica se i <= 15
	add $t0, $t0, $t1		# s = s + i
	
	addi $a0, $t1, 0		# coloca o valor de i em a0
	li $v0, 1			# imprime i na tela
	syscall
	
	li $v0, 4			# imprime mensagem
	la $a0, pipe			# coloca um |
	syscall
	
	addi $a0, $t0, 0		# coloca o valor de i em a0
	li $v0, 1			# imprime i na tela
	syscall
	
	li $v0, 4			# imprime mensagem
	la $a0, nl			# pula linha
	syscall
	
	addi $t1, $t1, 1		# i++
	j for				# volta para o for
	
fimFor:
	nop
	## opcional
	#addi $a0, $t0, 0		# terminou for
	#li $v0, 1			# imprime resultado
	#syscall
	
# saida
# i | soma
# 5 | 5
# 6 | 11
# ...	