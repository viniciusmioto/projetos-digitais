# Exercício 2

# if (i < j) {
# 	j = 2*i + 8;
# } else {
#	j = i*8 + j*6 + 12;
# } 

# t0 -> variavel i
# t1 -> variavel j

.text
	
	addi $t0, $zero, 1		# i = 1
	addi $t1, $zero, 2		# j = 2

	bge $t0, $t1, else		# verifica se i >= j (else)
	sll $t2, $t0, 1			# i = i*2
	addi $t1, $t2, 8		# j = i*2 + 8

	j fim
	
else:
	sll $t2, $t0, 3			# i = i*8
	mul $t3, $t1, 6			# j = j*6
	add $t1, $t2, $t3		# j = i*8 + j*6
	addi $t1, $t1, 12		# j += 12
	
fim: 	
	addi $a0, $t1, 0		# coloca o valor de j em a0
	li $v0, 1			# imprime j na tela
	syscall
