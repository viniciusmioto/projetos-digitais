# main
.text
	addi $a0, $zero, 3		# parametro X = 3
	addi $a1, $zero, 5		# parametro Y = 5
	jal f1
	
	move $s0, $v0			# salva o resultado
	move $a0, $s0			# coloca o resultado de fat em a0
	li $v0, 1			# imprime resultado na tela
	syscall
	
	li $v0, 10			# encerra o programa
	syscall

.text
f1:
	addi $sp, $sp, -4		# reserva espaço na pilha
	sw $ra, 0($sp)			# aloca o ende de retorno $ra
	
	jal f2				# salta para a função f2	
	move $t0, $v0			# salva o resultado
	
	mul $a0, $a0, $a1		# x = x * y
	add $v0, $a0, $t0		# retorno = x * y + f2(x)
	
	lw $ra, 0($sp)			# carrega end de ret da pilha
	addi $sp, $sp, 4		# devolve espaço para a pilha
	
	jr $ra				# pula para o endereço de retorno


f2:
	mul $t1, $a0, 3			# t1 = a * 3
	addi $v0, $t1, 2		# retorno = t1 + 2
	jr $ra				# pula para o endereço de retorno