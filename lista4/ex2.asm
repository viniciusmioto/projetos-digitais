#main
.text
	addi $t1, $zero, 2		# x = 2
	addi $t2, $zero, 3		# y = 3
	addi $t3, $zero, 6		# z = 6
	
	move $a0, $t1			# parametro A = x
	mul $a1, $t1, $t3		# parametro B = x * z
	add $a2, $t1, $t2		# parametro C = x + y
	add $a2, $a2, $t3		# parametro C += z
	sub $a3, $t2, $t1		# parametro D = y - x
	add $a3, $a3, $t3		# parametro D += z
	jal f1				# salta para a função f1
	
	move $s0, $v0			# salva o resultado
	move $a0, $s0			# coloca o resultado de fat em a0
	li $v0, 1			# imprime resultado na tela
	syscall
	
	li $v0, 10			# encerra a execução
	syscall
	

.text
f1:
	addi $t0, $zero, 2		# r = 2
	
	mul $t0, $t0, $a1		# r = r * b
	add $t0, $t0, $a0		# r += a
	
	add $t0, $t0, $a2		# r += c
	add $t0, $t0, $a3		# r += d
	
	move $v0, $t0			# salva o retorno da função
	jr $ra				# salta para ender de retorno
	