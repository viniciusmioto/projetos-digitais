.text
	addi $a0, $zero, -2		# a = -1
	addi $a1, $zero, 3		# b = 3
	addi $a2, $zero, 5		# c = 5
	addi $a3, $zero, 7		# d = 7
	
	jal f1
	
	move $s0, $v0			# salva o resultado
	move $a0, $s0			# coloca o resultado de fat em a0
	li $v0, 1			# imprime resultado na tela
	syscall
	
	li $v0, 10			# encerra o programa
	syscall

f1:
	addi $t0, $zero, 2		# r = 2
	mul $t0, $t0, $a1		# r = r * b
	add $t0, $t0, $a0		# r += a
	add $t0, $t0, $a2		# r += c
	add $t0, $t0, $a3		# r += d
	
	bgt $t0, 120, true		# if (r > 120) -> true
	bltz $t0, true			# if (r < 0) -> true
	
	sub $a1, $t0, $a1		# B = r - b
	sub $a2, $t0, $a2		# C = r - c
	sub $a3, $t0, $a3		# D = r - d
	
	
	addi $sp, $sp, -4		# reserva espaço na pilha
	sw $ra, 0($sp)			# aloca o ende de retorno $ra
	
	jal f1
	
	lw $ra, 0($sp)			# carrega end de ret da pilha
	addi $sp, $sp, 4		# devolve espaço para a pilha
	
	jr $ra				# pula para o endr de retorno
	
	
true:
	move $v0, $t0			# retorna r
	jr $ra				# pula para o endr de retorno